# Fly Project Notes

### What I built and what I didn't

I added two more sections to the dashboard.

A new sidebar section with deployment status of builds.
The deployment status sidebar includes:
- Version, instance count statuses, description


A new body section with information about the instances.
This section includes
- Status info: is the instance failing/unhealthy along with a status icon
- Region with region image, the task and version

There is a show/hide button that gives a dropdown with even more info.
I tried to show the most important info (such as status) as first info the user sees.

### What I would fix or improve if I had my time

- Implementing site-wide state. A 'store' for live viewers that contains state.
  When a user visits any page load that data and keep it for subsequent renders.

- State would be pushed by the deployment system instead of querying every few seconds
  to see if the data has changed. Only query the API when data has actually changed.

- Better rendering, right now when a user visits a page there are empty components
  and others only pop in after the first render. I want to make this smoother

- Split into easier to manage components

- Better handling of fly workers

### How to determine if a features is successful

- Have a thread on the forum about UI/UX changes and requests for feedback.

- Look at logs where users visit the most (is one feature being used more than another?)

- Are people talking about how epic a feature is :^)